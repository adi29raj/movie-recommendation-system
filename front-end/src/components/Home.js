import React, { useEffect, useState } from 'react';
import Card from '../components/Card';
import '../css/home.css';
import '../css/search.css';
import { useNavigate } from "react-router-dom";
import {POPULAR_MOVIE_ID_LIST} from '../config'
import axios from "axios";
import {BASE_URL} from '../config';


import img1 from '../images/superman.jpeg';
import img2 from '../images/ironMan.jpeg';
import img3 from '../images/harry.jpeg';
import img4 from '../images/inception.jpeg';

// let movieDataSet = [
//   { name: "Super Man", img: img1 },
//   { name: "Iron Man", img: img2 },
//   { name: "Harry Potter", img: img3 },
//   { name: "Inception", img: img4 },
// ];

let movieDataSet = [];

function Home(props) {
  const [movieData, setMovieData] = useState(movieDataSet);
  const [recommendedMovies, setRecommendedMovies] = useState();
  const [searchValue, setSearchValue] = useState('');
  const navigate = useNavigate();

  const handleSearch = (val) => {
    setSearchValue(val);
    if (val == '') {
      setMovieData(movieDataSet);
    } else {
      const updatedList = movieDataSet.filter((mov) => mov.title.toLowerCase().includes(val.toLowerCase()));
      setMovieData(updatedList);
    }

  }
  useEffect(() => {
    if (!props.loggedInUser.loggedInUserId) {
      navigate('/signin');
      alert('Please login first!');

    }
  }, [props.loggedInUser.loggedInUserId])

  useEffect(()=>{
    axios.post(`${BASE_URL}/movie_id`,{
        movieIdList:POPULAR_MOVIE_ID_LIST
  }).then((response) => {
      if(response.data.status == true){
          setMovieData(response.data.movieList);
          movieDataSet = response.data.movieList;
      }else{
          alert(response.data.message)
      }
  });
  },[])

  const handleRecommendation = (movieData) => {
    axios.post(`${BASE_URL}/ratings`,{
      userId:props.loggedInUser.loggedInUserId,
      movieId:movieData.movieId,
      rating:5,
    }).then((response) => {
      if(response.data.status == true){
          const rList = response.data.movieList;
          rList.unshift(movieData);
          setRecommendedMovies(response.data.movieList);
          // movieDataSet = response.data.movieList; // to:do maybe add this to compledata set
          // movieDataSet= [...movieDataSet,...response.data.movieList]
      }else{
          alert(response.data.message)
      }
    });
  }

  return (
    <div className="AppJS">
      <div className='sWrap'>
        <div className='title'>MRS</div>
        <div className="search-wrapper">
          <div className="search-container">
            <input type="text" id="id" placeholder="Search..." value={searchValue} onChange={(e) => handleSearch(e.target.value)} />
            <div class="search"></div>
          </div>
        </div>
        <div className='loggedInUser'>{props.loggedInUser.loggedInUser}</div>
      </div>
      <br />
      <h2 className='topPicks'>TOP PICKS FOR YOU</h2>
      <div className="cardWrapper">
        {movieData.map((item, i) => <Card movieData={item} loggedInUser={props.loggedInUser} handleRecommendation={handleRecommendation} />)}
      </div>
      {recommendedMovies?.length && <>
      <br/>
      <br/>
      <h2 className='topPicks'>MORE MOVIES LIKE {recommendedMovies[0].title.toUpperCase()}</h2>
      <div className="cardWrapper">
        {recommendedMovies.map((item, i) => <Card movieData={item} loggedInUser={props.loggedInUser} handleRecommendation={handleRecommendation} />)}
      </div>
      <br/>
      <br/>
      </>}

    </div>
  );
}

export default Home;