import React,{useState} from 'react';
import signinImg from "../images/signin-image.jpg";
import '../css/signup.css';
import axios from "axios";
import { useNavigate } from "react-router-dom";
import {BASE_URL} from '../config';

function Signin(props) {
    const[userName,setUserName] = useState('');
    const[password,setPassword] = useState('');
    const navigate = useNavigate();

    const handleSignin = () => {
        axios.post(`${BASE_URL}/signin`,{
            userName,
            password,
        }).then((response) => {
            if(response.data.status == true){
                console.log({loggedInUser : response.data.userName, loggedInUserId : response.data.userId},response.data)
                props.setUserDetails({loggedInUser : response.data.userName, loggedInUserId : response.data.userId});
            }else{
                alert(response.data.message)
            }
        });
    }
    return (
        <section class="sign-in signup">
            <div class="container-signup">
                <div class="signin-content">
                    <div class="signin-image">
                        <figure><img src={signinImg} alt="sing up image"/></figure>
                        <a onClick={() => navigate("/signup")} class="signup-image-link">Create an account</a>
                    </div>

                    <div class="signin-form">
                        <h2 class="form-title">Sign in</h2>
                        <form method="POST" class="register-form" id="login-form">
                            <div class="form-group">
                                <label for="your_name"><i class="zmdi zmdi-account material-icons-name"></i></label>
                                <input type="text" name="your_name" id="your_name" placeholder="Your Name" value={userName} onChange={(e) => setUserName(e.target.value)}/>
                            </div>
                            <div class="form-group">
                                <label for="your_pass"><i class="zmdi zmdi-lock"></i></label>
                                <input type="password" name="your_pass" id="your_pass" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)}/>
                            </div>
                            {/* <div class="form-group">
                                <input type="checkbox" name="remember-me" id="remember-me" class="agree-term" />
                                <label for="remember-me" class="label-agree-term"><span><span></span></span>Remember me</label>
                            </div> */}
                            <div class="form-group form-button">
                                <input type="button" onClick={handleSignin} name="signin" id="signin" class="form-submit" value="Log in"/>
                            </div>
                        </form>
                        {/* <div class="social-login">
                            <span class="social-label">Or login with</span>
                            <ul class="socials">
                                <li><a href="#"><i class="display-flex-center zmdi zmdi-facebook"></i></a></li>
                                <li><a href="#"><i class="display-flex-center zmdi zmdi-twitter"></i></a></li>
                                <li><a href="#"><i class="display-flex-center zmdi zmdi-google"></i></a></li>
                            </ul>
                        </div> */}
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Signin;