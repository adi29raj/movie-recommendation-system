import React,{useState} from 'react';
import signupImg from "../images/signup-image.jpg";
import '../css/signup.css';
import axios from "axios";
import { useNavigate } from "react-router-dom";
import {BASE_URL} from '../config';


function Signup(props) {
    const[userName,setUserName] = useState('');
    const[email,setEmail] = useState('');
    const[password,setPassword] = useState('');
    const[cnfmPswd,setCnfmPswd] = useState('');
    const navigate = useNavigate();

    const handleSignup = () => {
        if(password != cnfmPswd){
            alert('Password doesnot match!');
            return ;
        }
        axios.post(`${BASE_URL}/signup`,{
            userName,
            email,
            password,
        }).then((response) => {
            if(response.data.status == true){
                props.setUserDetails({loggedInUser : response.data.userName, loggedInUserId : response.data.userId});
                // navigate("/home");
            }else{
                alert(response.data.message)
            }
        });
    }
    return (
        <section className="signup">
            <div className="container-signup">
                <div className="signup-content">
                    <div className="signup-form">
                        <h2 className="form-title">Sign up</h2>
                        <form method="POST" className="register-form" id="register-form">
                            <div className="form-group">
                                <label for="name"><i className="zmdi zmdi-account material-icons-name"></i></label>
                                <input type="text" name="name" id="name" placeholder="Your Name" value={userName} onChange={(e) => setUserName(e.target.value)} required/>
                            </div>
                            <div className="form-group">
                                <label for="email"><i className="zmdi zmdi-email"></i></label>
                                <input type="email" name="email" id="email" placeholder="Your Email" value={email} onChange={(e) => setEmail(e.target.value)} required/>
                            </div>
                            <div className="form-group">
                                <label for="pass"><i className="zmdi zmdi-lock"></i></label>
                                <input type="password" name="pass" id="pass" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} required/>
                            </div>
                            <div className="form-group">
                                <label for="re-pass"><i className="zmdi zmdi-lock-outline"></i></label>
                                <input type="password" name="re_pass" id="re_pass" placeholder="Repeat your password" value={cnfmPswd} onChange={(e) => setCnfmPswd(e.target.value)} required/>
                            </div>
                            <div className="form-group form-button">
                                <input type="button" name="signup" id="signup" className="form-submit" value="Register" onClick={handleSignup}/>
                            </div>
                        </form>
                    </div>
                    <div className="signup-image">
                        <figure><img src={signupImg} alt="sing up image" /></figure>
                        <a onClick={() => navigate("/signin")} className="signup-image-link">I am already member</a>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Signup;