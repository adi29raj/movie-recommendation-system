import React,{useState} from 'react';
import '../css/card.css';
import '../css/rating.css';
import axios from "axios";
import {BASE_URL} from '../config';

const parseData = (arr) => {
  return JSON.parse(arr).join(', ')
}

function parsing(x)
{
    let l=x.length;
    let y="";
    for(let i=0;i<l;i++)
    if(x[i]=='"'||x[i]=="'"||x[i]=="["||x[i]==']'||x[i]==",")
    continue;
    else
    y=y+x[i];
    return y;
}

function Card(props){
   const movieData = props.movieData;
   const [rating,setRating] = useState(3);

   const handleRatingClick = (rate) => {
    setRating(rate);
    axios.post(`${BASE_URL}/ratings`,{
      userId:props.loggedInUser.loggedInUserId,
      movieId:movieData.movieId,
      rating:rate,
    }).then((response) => {
      // if(response.data.status == true){
      //     const rList = response.data.movieList;
      //     rList.unshift(movieData);
      //     setRecommendedMovies(response.data.movieList);
      //     // movieDataSet = response.data.movieList; // to:do maybe add this to compledata set
      //     // movieDataSet= [...movieDataSet,...response.data.movieList]
      // }else{
      //     alert(response.data.message)
      // }
    });
   }
   
    return (
    <div className="container-card">
    <div className="cellphone-container">    
        <div className="movie">       
          {/* <div className="menu"><i className="material-icons"></i></div> */}
          <div className="movie-img" style={{  "background-image": `url(${movieData.posterPath})` }}></div>
          <div className="text-movie-cont">
            <div className="mr-grid">
              <div className="col1">
                <h1>{movieData.title.substring(0,16) }</h1>
                <ul className="movie-gen">
                  <li>PG-13  /</li>
                  {/* <li>2h 49min  /</li> */}
                  <li>{parseData(movieData.genres)}</li>
                </ul>
              </div>
            </div>
            <div className="mr-grid summary-row">
              <div className="col2">
                <h5>SUMMARY</h5>
              </div>
              <div className="col2">
                 <ul className="movie-likes">
                  <li><i className="material-icons">&#xE813;</i>{movieData.movieId % 100}</li>
                  <li><i className="material-icons">&#xE813;</i>{movieData.id % 100}</li>
                </ul>
              </div>
            </div>
            <div className="mr-grid">
              <div className="col1">
                <p className="movie-description">{parsing(movieData.overview.substring(0,500))}...</p>
              </div>
            </div>
            <div className="mr-grid actors-row">
              <div className="col1">
                <p className="movie-actors">{parseData(movieData.cast)}</p>
              </div>
            </div>
            <div className="mr-grid action-row wrap">
              <div className="col2"><button className="watch-btn" onClick={() => props.handleRecommendation(movieData)}><h3><i className="material-icons">&#xE037;</i>MORE LIKE THIS</h3></button>
              </div>
              <>
              {
                [...Array(rating).keys()].map((id,idx)=> <div className="col8 action-btn" onClick={() =>handleRatingClick(idx+1)}><i className="material-icons gold">&#9733;</i></div>)
              }
              {
                [...Array(5-rating).keys()].map((id,idx)=> <div className="col8 action-btn" onClick={() =>handleRatingClick(rating + idx+1)}><i className="material-icons grey">&#9733;</i></div>)
              }
             </>
            </div>
          </div>
        </div>
    </div>
  </div>
  );
  }
  
  export default Card;