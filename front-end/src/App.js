import React,{useEffect, useState} from 'react';
import { Routes, Route } from "react-router-dom";
import Signup from './components/Signup';
import Signin from './components/Signin';
import Home from './components/Home';
import { useNavigate } from "react-router-dom";

let LOGGEDINUSER = {};

function App(){
  const [loggedInUser,setLoggedInUser] = useState({});
  const navigate = useNavigate();

  const setLoggedInUserDetails = (userDetails) => {
    if(userDetails){
      LOGGEDINUSER = userDetails;
      setLoggedInUser(userDetails)
    }
  }

  useEffect(() => {
    if(loggedInUser?.loggedInUser){
      navigate('/home')
    }
  },[loggedInUser])

  return (
    // <BrowserRouter>/
      <Routes>
        <Route path="/" element={<Signin setUserDetails={(userDetails) => setLoggedInUserDetails(userDetails)}/>} />
        <Route path="/signin" element={<Signin setUserDetails={(userDetails) => setLoggedInUserDetails(userDetails)}/>} />
        <Route path="/signup" element={<Signup setUserDetails={(userDetails) => setLoggedInUserDetails(userDetails)}/>} />
        <Route path="/home" element={<Home loggedInUser={loggedInUser}/>} />
          {/* <Route path="*" element={<NoPage />} /> */}
      </Routes>
    // </BrowserRouter>
  );
}


export default App;
