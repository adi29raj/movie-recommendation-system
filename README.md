# To contribute to this project

Make a gitlab account
In your working directory 
1. Clone and set origin the repository with ssh or https
    git clone https://gitlab.com/adi29raj/movie-recommendation-system.git 
    or
    git clone git@gitlab.com:adi29raj/movie-recommendation-system.git

    git remote add origin https://gitlab.com/adi29raj/movie-recommendation-system.git

2. Create a new branch
    git checkout -b branchName

3. Install node_modules for both front-end and back-end
    cd back-end
    npm install
    cd front-end
    npm install

4. Run frontend and backend using
    npm start 
    and
    nodemon index.js

5. Make your changes

6. Add the changes to staging area
    git add .

7 Commit your changes
    git commit -m "commit message"

8 Push your change
    git push origin branchName

10 Go to already existing branch
    git checkout branchName